#include "get_next_line.h"

int main()
{
	char *line;
	int gnl;
	int fd;

	fd = open("file.txt", O_RDONLY);
	while (1)
	{
		gnl = get_next_line(fd, &line);
		free(line);
	}
}
