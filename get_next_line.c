/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/01 12:40:55 by dgoudet           #+#    #+#             */
/*   Updated: 2019/12/21 10:34:34 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	is_line(char *str)
{
	int i;

	i = 0;
	if (str[i] == '\n')
		return (i);
	while (str[i])
	{
		if (str[i] == '\n')
			return (i);
		i++;
	}
	return (-1);
}

char		*new_str(char *str, char *buf)
{
	char	*temp;

	if (is_line(str) == -1)
	{
		temp = ft_strdup(str);
		free(str);
		str = NULL;
		str = ft_strjoin(temp, buf);
		free(temp);
		temp = NULL;
		free(buf);
		buf = NULL;
		return (str);
	}
	else
	{
		buf = ft_strdup(str);
		free(str);
		str = NULL;
		str = ft_substr(buf, is_line(buf) + 1, ft_strlen(buf) - is_line(buf));
		free(buf);
		buf = NULL;
		return (str);
	}
}

int			gnl_return(char **str, char **line, char *buf, int ret)
{
	if (ret < 0)
	{
		free(buf);
		buf = NULL;
		return (-1);
	}
	if (is_line(*str) != -1)
	{
		*line = ft_substr(*str, 0, is_line(*str));
		*str = new_str(*str, buf);
		return (1);
	}
	if (str[0] == '\0')
		*line = ft_strdup("");
	else
		*line = ft_strdup(*str);
	free(*str);
	*str = NULL;
	return (0);
}

static char	*ft_malloc_buf(void)
{
	char *buf;

	if (!(buf = malloc(sizeof(*buf) * (BUFFER_SIZE + 1))))
		return (0);
	return (buf);
}

int			get_next_line(int fd, char **line)
{
	int			ret;
	char		*buf;
	static char	*str;

	if (!line || fd < 0)
		return (-1);
	ret = 1;
	if (!str)
	{
		buf = ft_malloc_buf();
		if ((ret = read(fd, buf, BUFFER_SIZE)) < 0)
			return (gnl_return(&str, line, buf, ret));
		buf[ret] = '\0';
		str = ft_strdup(buf);
		free(buf);
	}
	while (str && ((is_line(str)) == -1) && ret > 0)
	{
		buf = ft_malloc_buf();
		if ((ret = read(fd, buf, BUFFER_SIZE)) < 0)
			return (gnl_return(&str, line, buf, ret));
		buf[ret] = '\0';
		str = new_str(str, buf);
	}
	return (gnl_return(&str, line, buf, ret));
}
